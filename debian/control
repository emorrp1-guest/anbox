Source: anbox
Section: contrib/utils
Priority: optional
Maintainer: Shengjing Zhu <zhsj@debian.org>
Rules-Requires-Root: no
Build-Depends:
 cmake,
 debhelper (>= 11),
 libboost-filesystem-dev,
 libboost-iostreams-dev,
 libboost-log-dev,
 libboost-program-options-dev,
 libboost-system-dev,
 libcap-dev,
 libegl1-mesa-dev,
 libgles2-mesa-dev,
 libgmock-dev,
 libproperties-cpp-dev,
 libprotobuf-dev,
 libsdl2-dev,
 libsdl2-image-dev,
 libsystemd-dev,
 lxc-dev,
 protobuf-compiler,
 python3,
Standards-Version: 4.2.1
Homepage: https://anbox.io
Vcs-Git: https://salsa.debian.org/zhsj/anbox.git
Vcs-Browser: https://salsa.debian.org/zhsj/anbox

Package: anbox
Architecture: amd64 arm64 armhf
Built-Using:
 ${misc:Built-Using},
Depends:
 iptables,
 libegl1,
 libgles2,
 ${misc:Depends},
 ${shlibs:Depends},
 ${misc:LxcDepends},
Recommends:
 dbus-user-session,
Description: Android in a box
 Anbox is a container-based approach to boot a full Android system on a
 regular GNU/Linux system.
 .
 In other words: Anbox will let you run Android on your Linux system
 without the slowness of virtualization.
 .
 Anbox uses Linux namespaces (user, pid, uts, net, mount, ipc) to run a
 full Android system in a container and provide Android applications on any
 GNU/Linux-based platform.
 .
 The Android inside the container has no direct access to any hardware. All
 hardware access is going through the anbox daemon on the host. It reuses
 what Android implemented within the QEMU-based emulator for OpenGL ES
 accelerated rendering. The Android system inside the container uses
 different pipes to communicate with the host system and sends all hardware
 access commands through these.
 .
 This package needs Android kernel modules and rootfs image, see
 /usr/share/doc/anbox/README.Debian for information.
